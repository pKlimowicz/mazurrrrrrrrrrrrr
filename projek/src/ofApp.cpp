//
// Copyright (c) 2014 Christopher Baker <https://christopherbaker.net>
//
// SPDX-License-Identifier: MIT
//
#include <string>
#include "ofApp.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_BLINK_FRAMES 28
#define MAX_DANCE_FRAMES 35

int FRAME = 0;
int MY_FRAME = 0;
bool waitWithBlink = false;
int waitBlinkCnt = 0;
int cmp = 10;

static void drawFirstMarker(int i, const ofxCvContourFinder *contours) {
	ofSetColor(255, 255, 255);
	ofFill();
	ofDrawLine(contours->blobs[i].centroid.x - 40, contours->blobs[i].centroid.y, contours->blobs[i].centroid.x + 40, contours->blobs[i].centroid.y);
	ofDrawLine(contours->blobs[i].centroid.x, contours->blobs[i].centroid.y - 40, contours->blobs[i].centroid.x, contours->blobs[i].centroid.y + 40);
}

static void fillBlinking(std::vector<ofImage> &v) {
	std::string path = "mruganie/";
	for (int i = 0; i < MAX_BLINK_FRAMES; ++i) {
		std::string p = path + to_string(i);
		ofImage tmp;
		//std::cout << "PATH: " << p << "\n";
		tmp.loadImage(p);

		v.push_back(tmp);
	}
}

static void fillMyDance(std::vector<ofImage> &v) {
	std::string path = "dance/";
	for (int i = 0; i < MAX_DANCE_FRAMES; ++i) {
		std::string p = path + to_string(i);
		ofImage tmp;
		tmp.loadImage(p);

		v.push_back(tmp);
	}
}

void ofApp::setup() {
	grabber.setGrabber(std::make_shared<ofxPS3EyeGrabber>());
	w = 640;
	h = 480;
	grabber.setup(w, h);
	grabber.getGrabber<ofxPS3EyeGrabber>()->setBrightness(40);
	rgb.allocate(w, h);
	hsb.allocate(w, h);
	hue.allocate(w, h);
	sat.allocate(w, h);
	bri.allocate(w, h);
	filtered.allocate(w, h);

	fillMyDance(klimko);
	peppaPig.loadImage("images/peppa.png");
	fillBlinking(peppaBlink);
}

void ofApp::update() {
	srand(time(0));
	grabber.update();
	if(grabber.isFrameNew()) {
		rgb.setFromPixels(grabber.getPixels());
		rgb.mirror(false, false);
		hsb = rgb;
		hsb.convertRgbToHsv();
		hsb.convertToGrayscalePlanarImages(hue, sat, bri);

		for (int i=0; i<w*h; i++) {
			filtered.getPixels()[i] = ofInRange(hue.getPixels()[i],findHue-5,findHue+5) ? 255 : 0;
		}

		filtered.flagImageChanged();
		contours.findContours(filtered, 300, w*h/3, 1, false);
	}
}

void ofApp::draw() {
	ofSetColor(255,255,255);
	//draw all cv images
	rgb.draw(0,0);
	//hsb.draw(640,0);
	//hue.draw(0,240);
	//sat.draw(320,240);
	//bri.draw(640,240);
	//filtered.draw(0,480);
	//contours.draw(640, 0);

	for (int i = 0; i < contours.nBlobs; i++) {
		if (chooseFirstMarker) {
			drawFirstMarker(i, &contours);
		} else if (drawMe) {
			for (int j = 0; j < 100; ++j)
				klimko[MY_FRAME].draw(contours.blobs[i].centroid.x - 200, contours.blobs[i].centroid.y - 400, 400, 500);
			MY_FRAME++;
			if (MY_FRAME == MAX_DANCE_FRAMES) {
				MY_FRAME = 0;
			}
		} else if (drawPeppaPig) {
			peppaPig.draw(contours.blobs[i].centroid.x - 130, contours.blobs[i].centroid.y - 350, 250, 350);
		} else if (blink) {
			peppaBlink[FRAME].draw(contours.blobs[i].centroid.x - 130, contours.blobs[i].centroid.y - 350, 250, 350);
			FRAME++;

			if (FRAME == MAX_BLINK_FRAMES) {
				FRAME = 0;
				waitWithBlink = true;
				blink = false;
			}
		} else if (waitWithBlink && !drawMe && !drawPeppaPig && !blink) {
			peppaBlink[1].draw(contours.blobs[i].centroid.x - 130, contours.blobs[i].centroid.y - 350, 250, 350);

			waitBlinkCnt++;
			if (waitBlinkCnt == cmp) {
				waitWithBlink = false;
				blink = true;
				waitBlinkCnt = 0;
				cmp = rand() % 121 + 10;
			}
		}
	}
}

void ofApp::keyPressed(int key) {
	switch(key) {
		case 'r':
			findHue = 0;
			drawPeppaPig = false;
			drawMe = false;
			chooseFirstMarker = true;
			blink = false;
			break;
		case 't':
			drawMe = true;
			chooseFirstMarker = false;
			drawPeppaPig = false;
			blink = false;
			break;
		case 'p':
			drawMe = false;
			chooseFirstMarker = false;
			drawPeppaPig = true;
			blink = false;
			break;
		case 'b':
			blink = true;
			drawPeppaPig = false;
			drawMe = false;
			chooseFirstMarker = false;
			break;
		case 'h':
			std::cout << "HELP SECTION\n";
			std::cout << "t - draw Patryk on marker.\n";
			std::cout << "p - draw Peppa pig on marker.\n";
			std::cout << "r - stop drawing objects, start follow marker.\n";
			std::cout << "b - to see how Peppa pig is blinking.\n";
			break;
		default:
			std::cout << "hit 'h' for help.";
	}
}

void ofApp::mousePressed(int x, int y, int button) {
	int mx = x % w;
	int my = y % h;

	if (findHue == 0 && chooseFirstMarker) {
		findHue = hue.getPixels()[my*w+mx];
		chooseFirstMarker = true;
	}
}

