//
// Copyright (c) 2014 Christopher Baker <https://christopherbaker.net>
//
// SPDX-License-Identifier: MIT
//

#pragma once

#include "ofMain.h"
#include "ofxPS3EyeGrabber.h"
#include "ofxOpenCv.h"
#include <vector>

class ofApp: public ofBaseApp {
public:
	void setup() override;
	void update() override;
	void draw() override;
	void keyPressed(int key);
	void mousePressed(int x, int y, int button);

	ofVideoGrabber grabber;
	ofVideoGrabber movie;

	ofxCvColorImage rgb, hsb, hsb2;
	ofxCvGrayscaleImage hue, hue2, sat, bri, filtered, filtered2;
	ofxCvContourFinder contours, contours2;

	std::vector<ofImage> klimko;
	ofImage peppaPig;
	std::vector<ofImage> peppaBlink;
	vector<ofImage> peppaWalk;

	int w, h;
	int findHue = 0;
	int findHue2 = 0;
	bool drawMe = false;
	bool chooseSecondMarker = false;
	bool chooseFirstMarker = true;
	bool drawPeppaPig = false;
	bool blink;
	bool walk;
};

